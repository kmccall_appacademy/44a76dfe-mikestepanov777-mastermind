  class Code
  attr_reader :pegs
  PEGS = {red: "R", green: "G", blue: "B", yellow: "Y", orange: "O", purple: "P"}

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(pegs)
    raise "Exit error. You must type 4 letters only!" if pegs.scan(/[RGBYOP]/i).size != 4
    pegs = pegs.chars.map{|chr| PEGS.key(chr.upcase)}
    Code.new(pegs)
  end

  def self.random
    pegs = []
    4.times {pegs << PEGS.keys.sample}
    Code.new(pegs)
  end

  def[](i)
    @pegs[i]
  end

  def exact_matches(other_pegs)
    exact = 0
    (0...4).each do |idx|
      exact += 1 if self[idx] == other_pegs[idx]
    end
    exact
  end

  def near_matches(other_pegs)
    banned = []
    pegs_arr = []
    other_arr = []
    (0...4).each do |idx|
      banned << self[idx] if other_pegs[idx] == self[idx]
      pegs_arr << self[idx]
      other_arr << other_pegs[idx]
    end
    ((pegs_arr & other_arr) - banned).size
  end

  def ==(other_pegs)
    return false unless other_pegs.class == Code
    self.pegs == other_pegs.pegs
  end
end

class Game
  attr_reader :secret_code

  def initialize(secret_code = nil)
    secret_code = Code.random if secret_code.nil?
    @secret_code = secret_code
    @turns = 10
  end

  def play
    until @turns == 0
      puts "You have #{@turns} turns left."
      guess = get_guess
      break if @secret_code == guess
      display_matches(guess)
      @turns -= 1
    end
    puts "The correct code was #{@secret_code.pegs}."
    if @turns > 0
      puts "You won!"
    else
      puts "You ran out of turns! So sad!"
    end
  end

  def get_guess
  puts "Write down 4 letters of combination of [R, G, B, Y, O, P]."
    begin
      guess = Code.parse(gets.chomp)
    rescue
      puts "Enter a valid code:"
      retry
    end
    guess
  end

  def display_matches(guess)
    exact = @secret_code.exact_matches(guess)
    near = @secret_code.near_matches(guess)
    puts "Your guess has #{exact} exact and #{near} near matches."
  end
end

if __FILE__ == $PROGRAM_NAME
  Game.new.play
end
